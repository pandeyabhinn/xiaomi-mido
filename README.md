Ubuntu Touch for Redmi Note 4

Device Checklist
================

Working
-------
* Actors: Torchlight 
* Actors: Vibration
* Bluetooth: Driver loaded at startup
* Bluetooth: Enable/disable and flightmode works
* Bluetooth: Persistent MAC address between reboots
* Cellular: Carrier info, signal strength
* Cellular: Change audio routings (Speakerphone, Earphone)
* Cellular: Data connection
* Cellular: Enable/disable mobile data and flightmode works
* Cellular: Incoming, outgoing calls
* Cellular: PIN unlock
* Cellular: SMS in, out
* Cellular: Voice in calls
* GPU: Boot into SPinner animation and Lomiri UI
* Misc: AppArmor patches applied to kernel
* Misc: Battery percentage
* Misc: Date and time are correct after reboot 
* Misc: Online charging (Green charging symbol, percentage increase in stats etc)
* Misc: SD card detection and access
* Misc: Shutdown / Reboot
* Sensors: Rotation works in Lomiri
* Sensors: GPS
* Sensors: Touchscreen registers input across whole surface
* Sound: Loudspeaker, volume control ok
* USB: MTP access
* WiFi: Driver loaded at startup
* WiFi: Enable/disable and flightmode works


Not tested
-----------------------------

* Bluetooth: Pairing with headset works, volume control ok
* Cellular: MMS in, out
* Cellular: Switch connection speed between 2G/3G/4G works for all SIMs
* Cellular: Switch preferred SIM for calling and SMS - only for devices that support it
* Endurance: Battery lifetime (Not measured yet)
* Endurance: No reboot needed for 1 week
* GPU: Hardware video decoding
* Misc: Offline charging (Power down, connect USB cable, device should not boot to UT) (Boots into fastboot)
* Sensors: Proximity works during a phone call
* Sensors: Fingerprint reader, register and use fingerprints (Halium >=9.0 only)
* Sound: Earphones detected, volume control ok
* Sound: Microphone, recording works
* Sound: System sounds and effects plays correctly (Camera shutter, Screenshot taken, Notifications)
* WiFi: Hotspot can be configured, switched on and off, can serve data to clients
* WiFi: Persistent MAC address between reboots
* USB: ADB access

Not working
-----------
* Actors: Manual brightness
* Actors: Notification LED
* Camera: Ensure proper cameras are in use (On device with more than 2 logical cameras)
* Camera: Flashlight
* Camera: Photo
* Camera: Switch between back and front camera
* Camera: Video
* Misc: Reset to factory defaults
* Misc: logcat, dmesg & syslog do not spam errors and service restarts (Some errors are still shown in dmesg and syslog that needs to be checked if they are fatal)
* Misc: Recovery image builds and works
* Misc: Anbox patches applied to kernel
* Sensors: Automatic brightness
